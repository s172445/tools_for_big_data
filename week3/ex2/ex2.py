import numpy as np
from scipy.interpolate import interp1d

import matplotlib.pyplot as plt

f = open('points.txt', 'r')
x = []
y = []

for line in f:
    tmp = line.split()
    x.append(int(tmp[0]))
    y.append(float(tmp[1]))

cubic_interp = interp1d(x, y, kind='cubic')

xnew = np.arange(-20, 19, 0.1)
ynew = cubic_interp(xnew)   # use interpolation function returned by `interp1d`
plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()