import numpy as np
import numpy.linalg as linalg

matrix = np.loadtxt('file.txt', delimiter=',')
A = []
B = []

dimensions = list(matrix.shape)
nrOfRows = int(dimensions[0])
nrOfColumns = int(dimensions[1])

B = matrix[:,nrOfColumns - 1]
A = matrix[:, :nrOfColumns - 1]

print linalg.solve(A, B)