# Write a simple Python function for computing a sum with 10,000 terms (this should be around 1.644),
# 500 times in a row (to make the execution time measurable). Now compile the code with Cython and
# see how much speedup you can achieve by this (use the time command on mac / linux for this).
# How many times faster is the Cython version? Remember to declare your variable types.

import time
from comfunc import computesum

# Simple Python function for computing the sum


nr_of_terms = int(input("Number of terms: "))

start_time = time.time()

# Call Python function 500 times in a row
for i in range (0, 500):
  print(computesum(nr_of_terms))

# Print execution time
print("Execution time: %s seconds" %(time.time() - start_time))

