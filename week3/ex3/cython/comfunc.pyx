def computesum(nr_of_terms):
  sum = 0.0
  for i in range(1, nr_of_terms + 1):
    sum += 1.0 / i ** 2
  return sum