from distutils.core import setup
from Cython.Build import cythonize


setup(
  name = 'compute func',
  ext_modules = cythonize("comfunc.pyx"),
)