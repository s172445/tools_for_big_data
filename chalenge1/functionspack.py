import xml.etree.cElementTree as etree
import string
import subprocess 
import time

pattern = ["best", (0,20),"to"]
namespace = {'mw': 'http://www.mediawiki.org/xml/export-0.10/'}


#def fixtag(str ns,str tag, nsmap):
#    return '{' + nsmap[ns] + '}' + tag

# Teacher's recursive function searching for query 
def get_matches_starting_at(data, pattern, index, starting_index, resulting_matches):
    sub_data = data[index:]

    P = pattern[0]
    if sub_data[:len(P)] == P:
        if len(pattern) == 1:
            resulting_matches.append((starting_index, index + len(P)))
        else:
            for new_index in range(pattern[1][0] + 1, pattern[1][1] + 2):
                get_matches_starting_at(data, pattern[2:], index + len(P) + new_index - 1, starting_index,
                                        resulting_matches)

        return resulting_matches
    else:
        return []

# Teacher's qunctions function searching for query 
def get_all_matches(data, pattern):
    matches_found = []
    for index in range(len(data)):
        matches_at_index = get_matches_starting_at(data, pattern, index, index, [])
        matches_found += matches_at_index

    return set(matches_found)

# Parse xml-wiki file and create clean file in format with free columns separeted by tab \t: ID\tName\tText-of-article
def parseAndSave( imputfile, outputfile, namespace):
    #cdef str title, id, readtext
    file = open(outputfile,'w')
    results = []
    nsmap = {}
    for event, elem in etree.iterparse(imputfile, events=('start', 'end', 'start-ns', 'end-ns')):
        #if event == 'start-ns':
           # if type(elem) != type(None):
           #     ns, url = elem
           #     nsmap[ns] = url
           #     #print '--%s==' % nsmap.keys()[0]
        if event == 'end':
            if elem.tag == '{http://www.mediawiki.org/xml/export-0.10/}page':
                title = elem.find('.//mw:title', namespace).text.encode('utf-8')
                id = elem.find('.//mw:id', namespace).text.encode('utf-8')
                textelem = elem.find('.//mw:text', namespace)
                #print type(textelem)
                if textelem is not None and textelem.text is not None:
                    #print id, title, textelem
                    readtext=textelem.text.encode('utf-8')
                    if string.find(readtext, "#REDIRECT") == -1:
                        #print id + '\t' + title + '\t' + readtext.lower().replace('\n', ' ') + '\n'
                        file.write(id+'\t'+title+'\t'+readtext.lower().replace('\n', ' ') + '\n')
                elem.clear()
    file.close()




#Not finisehed
# Parse xml-wiki file and create clean file in format with free columns separeted by tab \t: ID\tName\tText-of-article
# In meantime it search for queries given by patern
def parseAndSaveSearch( imputfile, outputfile, pattern):
    #cdef str title, id, readtext
    file = open(outputfile,'w')
    results = []
    nsmap = {}
    finaltext = ''
    foundPatterns = []
    for event, elem in etree.iterparse(imputfile, events=('start', 'end', 'start-ns', 'end-ns')):
        #if event == 'start-ns':
           # if type(elem) != type(None):
           #     ns, url = elem
           #     nsmap[ns] = url
           #     #print '--%s==' % nsmap.keys()[0]
        if event == 'end':
            if elem.tag == '{http://www.mediawiki.org/xml/export-0.10/}page':
                title = elem.find('.//mw:title', namespace).text.encode('utf-8')
                id = elem.find('.//mw:id', namespace).text.encode('utf-8')
                textelem = elem.find('.//mw:text', namespace)
                #print type(textelem)
                if textelem is not None and textelem.text is not None:
                    readtext=textelem.text.encode('utf-8')
                    if string.find(readtext, "#REDIRECT") == -1:
                        
                        cleantext = readtext.lower().replace('\n', ' ')
                        
                        matches = get_all_matches(cleantext, pattern)
                        for m in matches:
                            print [id, cleantext[m[0]:m[1]]]
                            foundPatterns.append([id, cleantext[m[0]:m[1]]])
                                    #id+'\t'+title+'\t'+readtext.lower().replace('\n', ' ') + '\n'
                        finaltext = id + '\t' + title + '\t' + cleantext + '\n'

                        
                        file.write(finaltext)
                elem.clear()
    file.close()

def getIdAndName(text):
    idindex = string.find(text,"\t")
    idd = text[:idindex]
    tindex = string.find(text,"\t",idindex+1)
    title = text[idindex+1:tindex]
    return idd, title, tindex+1

# From clean file it finds line which starts with ID
def findTextId(id, filename):
    command = 'grep "^' + id +'" -m 1 ' + filename
    text = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()
    #idd= re.search(r'^[0-9]+', text)
    idd, title, textStart = getIdAndName(text)

    return [idd, title, text[textStart:]]
     


def findTextName(startstring, filename):
    start_time = time.time() 
    print "starting preprocesing letterQuery"
    command = 'grep "^[0-9]*\t' + startstring.lower() +'" -i ' + filename + ' > tmpArticleQuery2' + startstring
    #print command
    subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()
    end_time = (time.time() - start_time)
    print "preprocesing finished time: ", end_time
    return 'tmpArticleQuery' + startstring

#findTextName('a','cleanwiki_large')
def singleArticleSearch(text, queries):
    for query in queries:
        start_time = time.time()
        print "Starting query for ", query
        index = 1
        matches = get_all_matches(text,query)
        print "Got ", len(matches), " matches in: ", (time.time() - start_time)
        for m in matches:
            print index, text[m[0]:m[1]]
            index +=1
        print ''

        
# supper slow runnig on all articles starting on A would take cca 2 hours
def letterArticleSearch(filename,query):
    print "Starting query for ", query
    with open(filename) as FileObj:
        start_time = time.time()
        resultF=[]
        for line in FileObj:
            idd, title, textStart = getIdAndName(line)
            text = line[textStart:]
            matches = get_all_matches(text,query)

            if len(matches) > 0:
                #print matches
                for m in matches:
                    resultF.append([idd, line[m[0]:m[1]]])
        print "Got ", len(resultF), " matches in: ", (time.time() - start_time)
        print resultF
        index = 1
        for result in resultF:
            print index, result[0], result[1]
            index += 1

    


