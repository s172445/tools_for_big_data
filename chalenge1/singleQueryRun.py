import xml.etree.cElementTree as etree
import functionspack as FP

wikifile = "cleanwiki_large"

# cats
queries = [["cat",[0,10],"are",[0,10],"to"], ['cat', (0, 100), 'anatomy'], ['china', (30, 150), 'washington'], ['english', (0, 200), 'cat'], ['kitten', (15, 85), 'cat', (0, 100), 'sire', (0, 200), 'oxford']]

text = FP.findTextId('6678',wikifile)

FP.singleArticleSearch(text[2], queries)